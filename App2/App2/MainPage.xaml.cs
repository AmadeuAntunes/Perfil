﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace App2
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        private void Button_OnClicked(object sender, EventArgs e)
        {
            LblFollowers.Text = (int.Parse(LblFollowers.Text) + 1).ToString();
        }

        private async void Menu_OnClick(object sender, EventArgs e)
        {
            ToolbarItem tb1 = (ToolbarItem)sender;
            await Application.Current.MainPage.DisplayAlert("Selecionou", 
                                                            tb1.Text, "OK");

            if(tb1.Text == "Edit")
            {
                await Application.Current.MainPage.Navigation.PushAsync(new PageDemo());

            }

        }

    }
}
