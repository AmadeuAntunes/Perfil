﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageDemo : ContentPage
    {
        public PageDemo()
        {
            InitializeComponent();
        }
        private void Slider_OnValueChanged(object sender, ValueChangedEventArgs e)
        {
            LblStatus.Text = String.Format("Valor Selecionado " + e.NewValue.ToString("0"));
        }
        private void Stepper_OnValueChanged(object sender, ValueChangedEventArgs e)
        {
            LblStepper.Text = String.Format("Valor Selecionado " + e.NewValue.ToString("0"));
        }

        private void toggle_OnValueChanged(object sender, ToggledEventArgs e)
        {
           
                Lbltoggle.Text =  e.Value?"Ligado":"Desligado";
        }
        private async void Button_OnClicked (object send , EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PushAsync(new ProximoPage());
        }

    }
}