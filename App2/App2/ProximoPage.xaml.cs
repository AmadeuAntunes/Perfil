﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProximoPage : ContentPage
    {


        public ProximoPage()
        {
            InitializeComponent();
        }


        private async void TxtUsername_OnCompleted(object sender, EventArgs e)
        {
            string username = TxtUsername.Text;
            if ((username.IndexOf("@") == -1))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Email invalido", "ok");
            }
        }

        private void TxtPassword_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            string senha = TxtPassword.Text;
            if (!(senha.Length > 5))
            {

                btnLogin.IsEnabled = true;
            }
            else
            {
                btnLogin.IsEnabled = false;
            }
        }

        private async void BtnLogin_OnClicked(object sender, EventArgs e)
        {
            string username = TxtUsername.Text;
            string password = TxtPassword.Text;

            if (!username.Equals("amadeu@amadeu.com") && password.Equals("123"))
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Email invalido", "ok");
            }
            else
            {
                await Application.Current.MainPage.Navigation.PopToRootAsync();

            }



        }
        private void BtnEnviar_onClicked(object sender, EventArgs e)
        {
            LblStatus.Text = TxtDescricao.Text;


        }
        private void SearchB_OnTextChanged(object sender, TextChangedEventArgs textChangedEventArgs)
        {
            LblStatus.Text = TxtDescricao.Text;
        }
        private void SearchB_OnButtonPressed(object sender, SearchBar e)
        {
            LblStatus.Text = TxtDescricao.Text;
        }
        private  void DataP_OnSelected(object sender, DatePicker e)
        {
            LblStatus.Text = DataP.Date.ToString("dd/MM/yyyy");
          
        }
       
    }
}